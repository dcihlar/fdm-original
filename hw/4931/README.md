## 4931

60kHz pilot injector/extractor and signal splitter.

It consists of two boards - one for each direction.

### Connections

```
4931A.16 = +

4931A.3  = in1+
4931A.4  = in1-
4931A.5  = in2+
4931A.6  = in2-
4931A.7  = in3+
4931A.8  = in3-
4931A.9  = in4+
4931A.10 = in4-
4931A.11 = in5+
4931A.12 = in5-

4931A.27 = pilot+1
4931A.28 = pilot-
4931A.29 = pilot+2

4931A.19 = out+1
4931A.20 = out-
4931A.21 = out+2


4931B.18 = +

4931B.31 = out1+
4931B.30 = out1-
4931B.29 = out2+
4931B.28 = out2-
4931B.27 = out3+
4931B.26 = out3-
4931B.25 = out4+
4931B.24 = out4-
4931B.23 = out5+
4931B.22 = out5-

4931B.5  = pilot_detect+
4931B.6  = pilot_detect-

4931B.13 = in+1
4931B.14 = in-
4931B.15 = in+2
```

### Design

![Module Cover](4931-03-cover.lowres.jpg)

![PCB1 Component Side](4931-01-front.lowres.jpg)

![PCB2 Component Side](4931-11-front.lowres.jpg)

![PCB1 Traces](4931-05-traces.lowres.jpg)

![PCB2 Traces](4931-15-traces.lowres.jpg)

![Schematic 1](4931-06-schematic.png)

![Schematic 2](4931-16-schematic.png)

