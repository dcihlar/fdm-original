## 4505

Merger for signals from 3115-3118 with pilot injection and detection.

A pilot signal of 84.08kHz is injected to the outgoing PG
and detected at the incoming PG.
The pilot is filtered out from the local signals (3115-3118).
2412 generates the stable pilot carrier for this module.

Incoming PG is just buffered,
but the outgoing PG can be adjusted by an operator.

There is high-pass output from buffered input remote signal available
at pins 11 and 13.
Unlike the ordinary output it doesn't have 84.08kHz removed
from the incoming PG.

Pilot signals as low as -6.26Npm0 (740µVrms) are detected and signaled
with relay B.
Detected pilot level can be measured with a module like 8626 or 8628.
It must be at least above 14, with optimum being 24 on the scale of 30.

When a pilot is lost or reacquired the alarm will be set or cleared
after a delay of about 1s.

Direction notation is in reference to PG.
![o->-](sym-output.svg) denotes outgoing PG (towards upstream), and
![o-<-](sym-input.svg) denotes incoming PG (from upstream).


### Specifications

 * Local impedance: 150Ω
 * Local input: -5.4Np (-46.9dB; roughly measured)
 * Local output: -4.4Np (-38.2dB; roughly measured)
 * Upstream input: -3.5Np (-30.4dB) / 150Ω
 * Upstream output: -4.2Np (-36.6dB) / 150Ω
 * Upstream input/output measurement: -5Np (-34.4dB) / 75Ω
   * Note: official documentation says: -6.1Np (-53dB) / 75Ω
 * Adjustable outgoing gain: ±0.1Np (±1dB)
   * Note: it affects the injected pilot level!
 * Pilot source: 84.08kHz, 0.25Vrms
 * Injecting pilot level: -2.3±0.23Npm0 (-20±2dB)
   * On the output it appears as -4.2 - 2.3 = -6.5Np (786µVrms)
   * On the input it appears as -3.5 - 2.3 = -5.8Np (1.17mVrms)
 * Detecting pilot (in reference to the expected level):
   * Lost: -0.81 - 1.04Np (7-9dB)
   * Reacquired: -0.69 - 0.46Np (6-4dB)
   * Delayed action: ≥1s

### Connections

```
4505.12 = +
4505.14 = VEE

4505.01 = relay
4505.02 = relay
4505.03 = relay
4505.04 = relay
4505.05 = relay

4505.06 = measure+
4505.07 = measure-

4505.09 = remote_in-
4505.10 = remote_in+

4505.11 = local_out_hp-
4505.13 = local_out_hp+

4505.15 = local_out+
4505.16 = local_out-

4505.17 = ? (internal connection is optional)

4505.18 = pilot+
4505.19 = pilot-

4505.20 = local_inA
4505.21 = local_in+
4505.22 = local_inB

4505.23 = remote_out+
4505.24 = remote_outA
4505.25 = remote_outB
```
