## 83024

This module has multiple functions:

 * power supply fuses,
 * carrier signals monitor,
 * a 3825Hz biasing transformer,
 * service phone.

A direct 4-wire (without hybrid) module containing only the microtelephone connector is 8316.

### Operation

#### Fuses

This module actually distributes the power supply through fuses to all subracks.
Each fuse can supply multiple different subracks.

#### Carrier signals monitor

The monitor consists of a reference voltage and a detector,
and it is the same as on 1415.
But unlike 1415 it doesn't latch the alarm state!

The power supply is also monitored.
If it drops below 20V, the alarm will be turned on.

#### Transformer

The transformer is used to bias 3825Hz carrier signal to -12V.
That enables turning the S tone on and off in the modulators.

The output from 1503 is feed into the transformer primary,
and the secondary is then connected to all modulator modules.

Its outer secondary poles are in 1:1 ratio,
and the inner poles are 4.5:1 ratio.

The same configuration can be found on 8324.

#### Service phone

A Microtelephone can be plugged in into the hinged slot.
It will get 24V power supply, 4-wire phone line, and input/output
ring signals.

Activating SK push button will place the module in "wait for call" mode.

When a ring signal (75 VAC) is detected, relay K1 will latch, light up SK
push button and activate P12.
User now knows that someone on the other side needs to talk.

Deactivating SK push button will turn off K1 and put the module in "talk" mode.

Now the phone line is connected to hybrid and user can talk with a person on
the other side.

In "talk" mode user can also generate ring signal for the other side.

This functionality is similar to 7301,
but only for one channel and with a builtin phone socket.


### Connections

```
83024N.12 = +
83024N.14 = VEE
83024P.16 = +
83024P.18 = VEE

83024N.01 = F1 VEE input
83024N.02 = F2 VEE input
83024N.03 = F3 VEE input
83024N.04 = F1 output
83024N.05 = F2 output
83024N.06 = F3 output
83024N.07 = F1-F3 fault

83024N.10 = voltage reference
83024N.19 = carriers state sense
83024N.22 = carriers OK
83024N.23 = carriers fault
83024N.25 = LED VEE

83024P.05 = service phone line A
83024P.06 = service phone line B

83024P.20 = 75 VAC L
83024P.21 = 75 VAC N

83024P.12 = service phone ring signal

83024P.13 = service phone hybrid tuning A
83024P.14 = service phone hybrid tuning B

83024P.08 = transformer VEE 1
83024P.09 = transformer VEE 2
83024P.10 = transformer VEE 3
83024P.23 = transformer primary A
83024P.24 = transformer primary B
83024P.27 = transformer secondary A
83024P.29 = transformer secondary B
83024P.26 = transformer secondary C
83024P.28 = transformer secondary D
```
