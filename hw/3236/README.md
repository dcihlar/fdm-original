## 3236 - 3240

These modules are V120/3 PG downconverters.

Shifted PG is mixed with a carrier signal and the result is normal PG.

Output gain is adjustable.

### Connections

```
3236.12 = +
3236.13 = VEE

3236.16 = carrier+
3236.17 = carrier-

3236.25 = in_A
3236.24 = in_B

3236.06 = PG_out_+
3236.07 = PG_out_A
3236.08 = PG_out_B
```

### Design

![Module Cover](3236-03-cover.lowres.jpg)

![PCB1 Component Side](3236-01-front.lowres.jpg)

![PCB1 Traces](3236-05-traces.lowres.jpg)

![Schematic 1](3236-06-schematic.png)

