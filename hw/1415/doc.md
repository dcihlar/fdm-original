## 1415

Carrier signals monitor.

A reference voltage is applied to all clock generator modules,
and the joined feedback is monitored.

An error is latched and signalled with a light bulb and outputted externally.
It can be reset into the non-fault state by pulsing EN signal high.
By default it is in the fault state.

The power supply (on the EN input) is also monitored.
If it drops below 20V, the alarm will be turned on.

There is also an optional filter that wasn't placed.

### Connections

```
1415.12 = +
1415.14 = VEE

1415.3  = ref_out
1415.5  = monitor_in
1415.11 = ok_out
1415.6  = fault_out
1415.1  = bulb_vee

1415.23 = filt_inA
1415.22 = filt_inB
1415.9  = filt_out+
1415.7  = filt_out-
```

