## 4508

PG 100× amplifier and attenuator.

4508 contains a power amplifier with 100× gain and an internally adjustable attenuator.

It is used to adapt a low power PG link to a high power PG link.
A high power PG link is useful for long distances or radio equipment that requires a higher amplitude.

Note: the same amplifier is used on 45008.

### Specifications

 * Input impedance: 160Ω
 * Input range: 4-400kHz, 120mVpp max
 * Gain: ×100
 * Power consumption: 1W

### Connections

```
4508.12 = +
4508.14 = VEE

4508.01 = out_A
4508.02 = out_B

4508.05 = atten_ioA
4508.06 = atten_ioB
4508.09 = atten_oiA
4508.10 = atten_oiB

4508.23 = in+
4508.25 = in-
```

