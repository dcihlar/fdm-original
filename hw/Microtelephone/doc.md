## Microtelephone

Plug in module for 8316 and 83024.

It has receive and transmit amplifiers,
ring lamp and ring key.

Two modes of operation are supported:

 * Mircotelephone mode - signals are received from backplane;
     this is the normal mode that should be used by default.
 * Modulator mode - signals are received from ISEP cable;
     used for checking individual 3016 .. 3018 signals.


### Specifications

 * TX: 1Npr (8.7dBr)
 * RX: -2Npr (-17.4dBr)


### Connections
```
2 = +
5 = VEE

1, 3 = microphone
7, 8 = speaker

4 = rx ring
6 = tx ring
```
