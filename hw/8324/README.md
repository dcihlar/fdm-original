## 8324

Fuses and monitoring.

9 fuses for supplying different subracks and other power supply consumers are available on the front panel,
and 4 more fuses can be added externally.
Every fuse has its own fault output.

Fuses are divided into groups.
Each group has its own VEE input.

With a 13-pole rotary switch each fuse can be selected for monitoring.
Two monitoring circuits are present on the module.
The second one has an external input.

There are also some additional features:

 * a 3825Hz biasing transformer like on 83024,
 * 5 differential ISEP test points,
 * two 6.5mm ISEP extension ports.

### Connections

```
8324.N06 = +
8324.N07 = +
8324.N03 = VEE
8324.P03 = VEE1
8324.P04 = VEE1
8324.P01 = VEE2
8324.P02 = VEE2
8324.P15 = VEE3
8324.P16 = VEE4

8324.P11 = F1_out
8324.N25 = F1_fault
8324.P10 = F2_out
8324.N26 = F2_fault
8324.P09 = F3_out
8324.N27 = F3_fault
8324.P08 = F4_out
8324.N28 = F4_fault
8324.P07 = F5_out
8324.N29 = F5_fault
8324.P06 = F6_out
8324.N31 = F6_fault
8324.P05 = F7_out
8324.N32 = F7_fault
8324.N15 = F8_out
8324.N20 = F8_fault
8324.N16 = F9_out
8324.N21 = F9_fault
8324.N17 = F10_ext
8324.N22 = F10_fault
8324.N14 = F11_ext
8324.N19 = F11_fault
8324.N13 = F12_ext
8324.N18 = F12_fault
8324.N12 = F13_ext
8324.N24 = F13_fault

8324.P14 = sel_sense_fault
8324.N01 = aux_sense
8324.N02 = 0_sense
8324.P17 = aux_fault

8324.P12 = sw1-13_in
8324.P13 = sw1-13_out

8324.P33 = test1_A
8324.P32 = test1_B
8324.P30 = test2_A
8324.P29 = test2_B
8324.P27 = test3_A
8324.P26 = test3_B
8324.P24 = test4_A
8324.P22 = test4_B
8324.P20 = test5_A
8324.P19 = test5_B

8324.N33 = transformer_VEE
8324.N30 = transformer_VEE2
8324.N23 = transformer_VEE3
8324.N08 = transformer_secondary_A
8324.N10 = transformer_secondary_B
8324.N09 = transformer_secondary_C
8324.N11 = transformer_secondary_D
8324.N04 = transformer_primary_A
8324.N05 = transformer_primary_B
```


### Design

![Module Cover](8324-03-cover.lowres.jpg)

![PCB1 Component Side](8324-01-front.lowres.jpg)

![PCB2 Component Side](8324-11-front.lowres.jpg)

![PCB1 Traces](8324-05-traces.lowres.jpg)

![PCB2 Traces](8324-15-traces.lowres.jpg)

![Schematic 1](8324-06-schematic.png)

