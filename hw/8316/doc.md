## 8316

This module only contains a 4-wire microtelephone connector
for service calls.

It is used by 7301 and 86001 is inserted in between.

Module 83024 has the same microtelephone connector,
but it also has a builtin hybrid.

### Connections
```
8316.05 = +
8316.06 = VEE

8316.03 = audio1_A
8316.07 = audio1_B

8316.09 = audio2_A
8316.10 = audio2_B

8316.04 = rx_ring
8316.08 = tx_ring
```
