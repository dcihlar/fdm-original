## 23015

PG line module with 60kHz pilot injector and detector.

Detects a pilot signal of 60kHz within a certain range.

Relay is activated only when the pilot signal is in the range.
If it's not in range, a fault is signaled by the lamp.

The received pilot level can be measured on the M port with 7108.
It must be between 15 and 30 on the scale of 30.

Input signal can be attenuated.
This will also affect the received pilot level.

23015 also injects pilot signal to output signal.
The output pilot amplitude can be adjusted.

Output signal is isolated, but directly forwarded.

60kHz signal is sourced from 2415.


### Specifications

 * Incoming PG gain: -0.4 - +0.25Np (-3.5 - +2.2dB)
 * Outgoing PG attenuation: -0.06Np (-0.5dB)
 * Acceptable incoming pilot levels:
   * Low: -5.1Np (2.3mVrms)
   * High: -4.2Np (5.6mVrms)
 * Injected pilot: -4.5±0.25Npm0 (4.3±1mVrms)
   * Note: this is the level on the outgoing PG


### Connections

```
23015.12 = +
23015.14 = VEE

23015.16 = tx_in_A
23015.17 = tx_in_B

23015.07 = tx_out_A
23015.08 = tx_out_B

23015.19 = pilot_A
23015.20 = pilot_B

23015.23 = rx_in_A
23015.25 = rx_in_B

23015.10 = rx_out_A
23015.11 = rx_out_B

23015.05 = fault_no_pilot

23015.01 = ?
23015.02 = ?
23015.03 = ?
23015.04 = ?
```

### Design

![Module Cover](23015-03-cover.lowres.jpg)

![PCB1 Component Side](23015-01-front.lowres.jpg)

![PCB2 Component Side](23015-11-front.lowres.jpg)

![PCB1 Traces](23015-05-traces.lowres.jpg)

![PCB2 Traces](23015-15-traces.lowres.jpg)

![Schematic 1](23015-06-schematic.png)

