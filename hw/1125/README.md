## 1125

540kHz generator.

### Operation

It multiplies the input frequency of 180kHz by 3 to get 540kHz.

### Connections

```
1125.12 = +
1125.14 = VEE

1125.1  = in-
1125.2  = in+

1125.18 = out+
1125.16 = out-
```

### Design

![Module Cover](1125-03-cover.lowres.jpg)

![PCB1 Component Side](1125-01-front.lowres.jpg)

![PCB1 Traces](1125-05-traces.lowres.jpg)

![Schematic 1](1125-06-schematic.png)

