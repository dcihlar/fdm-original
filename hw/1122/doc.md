## 1122

Final part of the oscillator.
It resumes division of the input frequency until it reaches 4000Hz.

1122 depends on 1003. See documentation of 1003 for more info.

It is further connected to 1120.

Pins 19 and 17 are usually tied together.
But they can also be used to get a DC biased output.


### Connections

For connections with 1003 see the documentation of 1003.

```
1122.12 = +
1122.14 = VEE

1122.21 → output+
1122.22 → output-

1122.19 = -10V_in
1122.17 = -10V_out
```
