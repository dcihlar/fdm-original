## 3242

Merging and splitting two PGs.

In the merged signal (PG24) PG1 is located at its original frequency range of 60-108kHz,
and PG2 is located in the lower range of 6-54kHz.

Carrier frequency is 114kHz generated from 12172.


### Specifications

PG1 range is starting from 56kHz, and PG2 range is up to 52kHz
(defined by high-pass and low-pass filters).


### Connections

#### N side

```
3242N.12 = +
3242N.14 = VEE

3242N.02 = PG1_inA
3242N.03 = PG1_inB
3242N.04 = PG1_in+

3242N.10 = PG2_in+
3242N.11 = PG2_in-

3242N.17 = carrier+
3242N.18 = carrier-

3242N.22 = PG24_out+
3242N.24 = PG24_outA
3242N.25 = PG24_outB
```

#### P side

```
3242P.12 = +
3242P.14 = VEE

3242P.04 = PG24_in+
3242P.06 = PG24_inA
3242P.07 = PG24_inB

3242P.16 = carrier+
3242P.17 = carrier-

3242P.19 = PG1_outA
3242P.20 = PG1_outB
3242P.22 = PG1_out+

3242P.22 = PG2_out+
3242P.24 = PG2_outA
3242P.25 = PG2_outB
```
