## 4202

The purpose of this module is _unknown_.

It's probably a complementary of 4104.

### Connections

```
4202.12 = +
4202.13 = VEE

4202.01 = in-
4202.02 = in+

4202.14 = out1-
4202.15 = out1+
4202.16 = out2-
4202.17 = out2+
4202.18 = out3-
4202.19 = out3+
4202.20 = out4-
4202.21 = out4+
4202.22 = out5-
4202.23 = out5+
4202.24 = out6-
4202.25 = out6+
```

### Design

![Module Cover](4202-03-cover.lowres.jpg)

![PCB1 Component Side](4202-01-front.lowres.jpg)

![PCB1 Traces](4202-05-traces.lowres.jpg)

![Schematic 1](4202-06-schematic.png)

