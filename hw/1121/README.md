## 1121

1121 is a part of the clock generator.

This board is similar to 1120.
Output signal appears to be triple the input frequency in AC spikes when disconnected from other modules.

Input signal is received from 12160 (pins 4 and 2),
or an external 12kHz signal.

It's further driving:

 * 12163
 * 12164
 * 12165
 * 12166
 * 1128

### Connections

As on 1120.

### Design

![Module Cover](1121-03-cover.lowres.jpg)

![PCB1 Component Side](1121-01-front.lowres.jpg)

![PCB1 Traces](1121-05-traces.lowres.jpg)

![Schematic 1](1121-06-schematic.png)

