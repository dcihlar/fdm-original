## 2303

60kHz pilot detector.

The relay signals the presence of the pilot signal.
It is closed immediately upon loss of the pilot,
and opened 1s after it is reacquired.

Received pilot level can be measured on a module like 86036 or 8628.
It must be at least 10, and probably less then 24 on the scale of 30.
Optimum is probably around 18.
Above 24 the metering circuit looses linearity.

The received pilot as a 60kHz sine wave can be obtained
on the front panel connector.
It can be used for local clock synchronization using Lissajous curves.


### Specifications

 * Input impedance: 75Ω
 * Pilot detection:
   * Low: -5.85Np (-50.8dB; 790µVrms)
   * High: -5.82Np (-50.6dB; 810µVrms)


### Connections

```
2303.12 = +
2303.14 = VEE

2303.1 = in_A
2303.2 = in_B

2303.11 = pilot_sine_out

2303.20 = has_pilot
2303.21 = has_pilot2
2303.22 = has_pilot2_in1
2303.25 = has_pilot2_in2

2303.23 = measure2 (AGC?)
2303.24 = pilot_measure
```

