## 3115 - 3118

Second level up/down-converters.

They are connected to first level converters 3016 - 3018.

Modules differ in carrier frequency:

 * 3115 = 120.00kHz
 * 3116 = 108.00kHz
 * 3117 =  96.00kHz
 * 3118 =  84.00kHz

### Operation

Input signal is shifted by carrier, and the mirror image is outputted.
So the input frequency is subtracted from the carrier frequency.

I.e. assuming 3016 - 3018 are connected to a 3115.
Frequency range from 3016 - 3018 is 12 - 24kHz and it will be shifted to 108 - 96kHz.
Note that the lower input frequency corresponds to the higher output frequency!

### Specifications

 * Local impedance: 600Ω
 * Local input: -4Np
 * Local output: -3.9Np
 * Upstream impedance: 150Ω
 * Upstream input: -4.4Np (-38.2dB; 4.65mVrms; roughly measured)
 * Upstream output: -5.4Np (-46.9dB; 1.8mVrms; roughly measured)

### Connections

```
3115.9  = +
3115.14 = VEE

3115.1  = lower out 1 B (shield)
3115.2  = lower out 1 A
3115.3  = lower out 2 B (shield)
3115.4  = lower out 2 A
3115.5  = lower out 3 B (shield)
3115.6  = lower out 3 A

3115.7  = higher in B (shield)
3115.8  = higher in A

3115.11 = carrier A
3115.13 = carrier B (shield)

3115.16 = lower in 1 A
3115.17 = lower in 1 B (shield)
3115.18 = lower in 2 A
3115.19 = lower in 2 B (shield)
3115.20 = lower in 3 A
3115.21 = lower in 3 B (shield)

3115.22 = higher out B (shield)
3115.23 = higher out A
```

### Design

![Module Cover](3115-03-cover.lowres.jpg)

![PCB1 Component Side](3115-01-front.lowres.jpg)

![PCB1 Traces](3115-05-traces.lowres.jpg)

![Schematic 1](3115-06-schematic.png)

