## 8515

System monitoring module.

Fuses, carriers, pilot signals and AGC state are monitored
and appropriate alarms are generated.

Two types of alarms are generated:

 * AL - carrier, AGC or system fuses error,
 * BL - system fuse, pilot, lamp fuse, or carrier error.

The rotary switch selects one of the alarming modes:

 * ON - normal mode,
 * B - BL disabled,
 * A/B - AL and BL disabled.

When not in normal mode the EL lamp is lit.

Some relay formulas are:

```
T = unknown purpose
R = fuse_error
Tr = carrier_error * !T
Uw = pilot_error * (!T + 1)
A = (carrier_ok + T) * !(pga_error * (!T + 1) + A_disable)
```

On the reverse engineered board `!T` was shorted by design so that's why there are `+ 1` in the formulas.
Also in reverse engineered subracks `A_disable` is connected to `R`.

### Connections

```
8515.P01 = +
8515.P05 = VEE
8515.P13 = VEE fused out

8515.N02 = pga_fault
8515.N05 = carrier_fault
8515.P11 = fuse_fault

8515.P02 = D1_A
8515.P10 = D1_K

8515.P06 = pilot_fault = Üw_en
8515.N07 = 8515.P06
8515.P03 = T_en
8515.N03 = T
8515.N04 = !T
8515.P15 = Üw
8515.P16 = R
8515.N30 = Tr
8515.P07 = AGC_error
8515.N24 = 8515.P07
8515.N28 = A_disable

8515.P28 = VEE fused out 2
8515.P29 = 8515.P28

8515.P32 = ?
8515.N27 = P32 * (SW@B + SW@ON)
8515.P33 = P32 * SW@ON

8515.P14 = SW@ON * !S
8515.P23 = (SW@ON + SW@B) * !A

8515.N26 = force_ampshadow_sw
8515.N31 = ampshadow_sw

8515.P22 = +
8515.P17 = SW@ON * R
8515.P18 = SW@ON
8515.P19 = alarm_BL$1 = SW@ON * (R + Üw + !S + Tr)
8515.P20 = alarm_BL$2
8515.P26 = alarm_BL + 8515.P27
8515.P27 = (SW@ON + SW@B) * !A
8515.N29 = SW@ON * Tr
8515.P21 = SW@ON * Üw 
8515.P24 = SW@ON * !S
8515.N32 = alarm_AL = !A * (SW@B + SW@ON)
8515.P30 = alarm_EL$1 = SW@A/B + SW@B
8515.P31 = alarm_EL$2
```


### Design

![Module Cover](8515-03-cover.lowres.jpg)

![PCB1 Component Side](8515-01-front.lowres.jpg)

![PCB2 Component Side](8515-11-front.lowres.jpg)

![PCB1 Traces](8515-05-traces.lowres.jpg)

![PCB2 Traces](8515-15-traces.lowres.jpg)

![Schematic 1](8515-06-schematic.png)

