## 8704

PG incoming line module with AGC and 84.08kHz pilot detector.

This module consists of two parts:

 * input buffer and 84.08kHz pilot signal detector,
 * AGC (automatic gain control).

It is used only for the incoming PG!
Outgoing PG is simply forwarded.
4505 must be used to inject the pilot signal into the outgoing PG.

Incoming PG, after the AGC, can be attenuated as desired.
This attenuation doesn't affect the pilot level.

AGC can be turned on or off by a switch or
even not used by the backplane at all.
When enabled it can be also adjusted manually.

The pilot level can be measured externally with a module like 8626.
Note that AGC affects the pilot level.
Measured level must be at least 14,
with optimum being 24 on the scale of 30.

AGC level can be also measured externally with 8626.
Signal `AGC_measure` is used to supply the AGC current,
and at the same time represent that current as a voltage for the level meter.

### Specifications

 * Manual gain adjustment: 0 - +0.3Np (0 - 2.6dB)
 * AGC range: -1.5 - 0Np (-13 - 0dB)

### Connections

```
8704.N12 = +
8704.N14 = VEE

8704.N8  = sw_in
8704.N25 = sw_out
8704.N13 = !rly_en

8704.N16 = AGC_measure
8704.N17 = AGC_out

8704.N18 = AGC_en
8704.N20 = AGC_sw_com
8704.N21 = AGC_sw_1
8704.N22 = AGC_sw_2

8704.N23 = pilot_low
8704.N24 = AGC_pilot_level


8704.P12 = +
8704.P14 = VEE

8704.P17 = PG_in_A
8704.P21 = PG_in_B

8704.P18 = PG_out_A
8704.P19 = PG_out_B

8704.P2  = pilot_detect_x1
8704.P3  = pilot_detect_x2
8704.P4  = pilot_detect_x3
8704.P5  = pilot_detect_x4
8704.P15 = pilot_detect_x5

8704.P9  = pilot_measure

8704.P1  = pilot_low
8704.P7  = AGC_pilot_level
8704.P23 = AGC_out
8704.P25 = AGC_measure
```

Example connection with AGC:
```
8704.N8  → +
8704.N13 → VEE
8704.N16 → 8704.P25
8704.N17 → 8704.P23
8704.N21 → 8704.N25
8704.N23 → 8704.P1
8704.N24 → 8704.P7
```


### Design

![Module Cover](8704-03-cover.lowres.jpg)

![PCB1 Component Side](8704-01-front.lowres.jpg)

![PCB2 Component Side](8704-11-front.lowres.jpg)

![PCB1 Traces](8704-05-traces.lowres.jpg)

![PCB2 Traces](8704-15-traces.lowres.jpg)

![Schematic 1](8704-06-schematic.png)

![Schematic 2](8704-16-schematic.png)

