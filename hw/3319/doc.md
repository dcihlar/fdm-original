## 3319

Incoming SG1 upconverter and amplifier.

This module is heavily shielded, and complementary to 3335.
It is similar to 3320, but with added upconverter.

The amplifier has a discretely adjustable gain.

The incoming signal is a lower part of SG1+2, and the output is normalized SG.
For more details about the conversion see 3335.

### Connections

```
3319.1  = + (chasis)
3319.2  = +
3319.12 = +
3319.14 = VEE
3319.25 = + (chasis)

3319.4  = out+
3319.5  = out-
3319.6  = out+

3319.16 = carrier+
3319.17 = carrier-
3319.18 = carrier+

3319.22 = in+
3319.23 = in-
3319.24 = in+
```
