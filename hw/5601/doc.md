## 5601

2-wire hybrid with attenuation.

It is pin-compatible with 4804.
It can also be rewired to act as a 4804.

There is also a patched variant marked as "ATA" which detects incoming bell by detecting
a short on the telephone pair.
Note that outgoing bell is still signalled using 75V AC.

### Configuration

#### Attenuation

On the top side of the PCB there are TX attenuators.
From left to right the attenuators are: -0.1Np, -0.2Np, -0.4Np, and -0.8Np.

On the bottom side of the PCB there are RX attenuators.
From left to right the attenuators are: -0.1Np, -0.2Np, -0.4Np, -0.8Np and -1.6Np.

Attenuators are enabled if the left hand side jumpers are shorted.
Right hand side jumpers disable the individual attenuators.
I.e. when jumpers 25 and 26 are shorted, and 29 and 30 are also shorted then
the -0.1Np TX attenuator is enabled.
With jumpers 26, 27, and 28, 29 shorted together the attenuator is disabled.

#### 2-wire

For 2-wire operation all right hand side jumpers in the range 1..24 need to be shorted.

On the original setup TX attenuation was set to -1.1Np,
and RX attenuation was set to -1.3Np.

With that configuration it had audio level of -0.1Np going to a telephone,
and expected -0.7Np from a telephone.
That gave the old microphones a bit of boost and leveled it with the speakers.

Modern telephones have built in microphone amplifiers
(sometimes even with a bit too high gain),
so the audio levels should be matched to -0.1Np.
Set TX attenuation to -1.1Np and RX attenuation to -1.9Np.


#### 4-wire

For 4-wire operation all left hand side jumpers in the range 1..24 need to be shorted.

The attenuation is arbitrary.

### Operation

Relay K1 signals outgoing (tx) bell, and relay K2 signals incoming (rx) bell signal.
75V AC is injected to 2-wire connection to send a bell signal.
Similarly a bell signal is detected.

2-wire signalling is compatible with POTS.
Only a DC power supply is missing.
It is possible to add it by connecting VCC and VEE to 2-wire output via two 1kΩ resistors.

### Connections

Note: external connections are connected to external components.
Internal connections are between individual modules.

For connections with 3016-3018 or the audio connectors see 4804.

```
5601.12 = +
5601.14 = VEE
5601.15 = 75V AC in L
5601.16 = 75V AC in N

5601.1  = internal bell rx
5601.2  = internal bell tx
5601.22 = external bell tx
5601.23 = external bell rx

5601.3  = external audio tx A
5601.4  = external audio tx B
5601.6  = internal audio tx B
5601.7  = internal audio tx A

5601.9  = internal audio rx A
5601.10 = internal audio rx B
5601.24 = external audio rx B
5601.25 = external audio rx A

5601.19 = hybrid impedance A
5601.20 = hybrid impedance B
```
