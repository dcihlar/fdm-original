#!/bin/bash

function phdr() {
	echo ==== Errors on $module ====
	numerrmods=$((numerrmods + 1))
	haserr=y
}

function pftr() {
	echo ==== end $module ====
	echo
}

function perr() {
	if [ $haserr = n ]
	then
		phdr
	fi
	numerrs=$((numerrs + 1))

	echo "  Missing: $1!"
}

echo '*******************'
echo '*                 *'
echo '*  Status report  *'
echo '*                 *'
echo '*******************'
echo

numerrs=0
numerrmods=0
for module in `find -maxdepth 1 -type d -printf '%P\n' | sort -n`
do
	haserr=n

	if [ -f $module/waiting.txt ]
	then
		phdr
		fold -w 70 -s $module/waiting.txt | sed -e 's/^/  /'
		pftr
		continue
	fi

	if [ -f $module/doc.md ]
	then
		if grep -q 'to be recovered' $module/doc.md
		then
			perr 'module'
			pftr
			continue
		fi
		grep -qi unknown $module/doc.md           && perr 'informations'
		grep -q TODO $module/doc.md               && perr 'work'
		grep -q '^### Connections' $module/doc.md || perr 'connections'
	else
		perr 'doc.md'
	fi

	[ -f $module/$module-01-front.jpg     ] || perr 'front image'
	[ -f $module/$module-02-back.jpg      ] || perr 'back image'
	[ -f $module/$module-03-cover.jpg     ] || perr 'cover image'
	[ -f $module/$module-04-components.*  ] || perr 'components'
	alltraces=( $module/$module-05-traces.* )
	[ -f ${alltraces[0]}                  ] || perr 'traces'
	[ -f $module/$module-06-schematic.png ] || perr 'schematic'

	[ $haserr = y ] && pftr
done

if [ $numerrs -gt 0 ]
then
	echo Found $numerrs errors >&2
	echo within $numerrmods modules! >&2
	exit 1
fi

exit 0
