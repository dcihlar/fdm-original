## 1267 - 1271

A range of boards for filtering frequencies from 1126.

They have a constant amplitude output and
a valid output detector.

 * 1267 - 420kHz
 * 1268 - 468kHz
 * 1269 - 516kHz
 * 1270 - 564kHz
 * 1271 - 612kHz


### Connections

```
1267.12 = +
1267.14 = VEE

1267.05 = 1267.06

1267.08 = in_A
1267.10 = in_B

1267.16 = measure-
1267.18 = measure+

12162.20 = out1-
12162.22 = out+
12162.23 = out+
12162.24 = out2-
12162.25 = en_out2
```

### Design

![Module Cover](1267-03-cover.lowres.jpg)

![PCB1 Component Side](1267-01-front.lowres.jpg)

![PCB1 Traces](1267-05-traces.lowres.jpg)

![Schematic 1](1267-06-schematic.png)

