## 4804

4-wire attenuator.

It is pin compatible with 5601.

Attenuation is selected by resoldering the attenuator component.

### Connections

Note 1: external connections are connected to external components.
Internal connections are between individual modules.

Note 2: only the first (out of four) audio link on the audio connector is displayed.
For other links the number must be increased by the number of the link.
Letter part of the pin name stays the same.

```
4804.12 = 3016.10 = +

4804.1  = 3016.18  = internal bell rx
4804.2  = 3016.9   = internal bell tx
4804.22 = audio.c7 = external bell tx
4804.23 = audio.c1 = external bell rx

4804.3  = audio.b7 = external audio tx A
4804.4  = audio.a7 = external audio tx B
4804.6  = 3016.1   = internal audio tx B
4804.7  = 3016.2   = internal audio tx A

4804.9  = 3016.24  = internal audio rx A
4804.10 = 3016.25  = internal audio rx B
4804.24 = audio.a1 = external audio rx B
4804.25 = audio.b1 = external audio rx A
```
