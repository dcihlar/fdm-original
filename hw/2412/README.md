## 2412

84.08kHz buffer with constant-amplitude output.

It also has a detector for valid input voltage.

Its source is 2108 and it is used by 4505 and 8704
to inject 84.08kHz pilot frequency into PGs.


### Connections

```
2412.12 = +
2412.14 = VEE

2412.5  = 2412.6

2412.7  = in-
2412.8  = in+

2412.16 = measure-
2412.18 = measure+

2412.20 = out1-
2412.21 = out2-
2412.22 = out+
2412.23 = out+
2412.24 = out3-
2412.25 = en_out3
```

### Design

![Module Cover](2412-03-cover.lowres.jpg)

![PCB1 Component Side](2412-01-front.lowres.jpg)

![PCB1 Traces](2412-05-traces.lowres.jpg)

![Schematic 1](2412-06-schematic.png)

