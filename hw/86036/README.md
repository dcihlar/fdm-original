## 86036

The purpose of this module is _unknown_.

### Connections

```
86036.12 = +
86036.14 = VEE

86036.01 = sw_in
86036.02 = sw_out1
86036.03 = sw_out2
86036.10 = sw_out3

86036.04 = rly

86036.25 = pilot measurement
```


### Design

![Module Cover](86036-03-cover.lowres.jpg)

![PCB1 Component Side](86036-01-front.lowres.jpg)

![PCB1 Traces](86036-05-traces.lowres.jpg)

![Schematic 1](86036-06-schematic.png)

