## 4104

The purpose of this module is _unknown_.

But it looks like it merges and amplifies output from PG upconverters.
It's probably complementary of 4202.

It could also be used to inject a pilot signal.

### Connections

```
4104.13 = VEE
4104.14 = +

4104.04 = pilot_A
4104.05 = pilot_B

4104.16 = in1-
4104.17 = in1+
4104.18 = in2-
4104.19 = in2+
4104.20 = in3-
4104.21 = in3+
4104.22 = in4-
4104.23 = in4+
4104.24 = in5-
4104.25 = in5+

4104.01 = out_A
4104.02 = out_B
```
