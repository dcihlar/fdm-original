## 7108

Embedded instrument for testing the rack with 800Hz AC generator.

It can measure:

 * AC and DC voltages on 30V or 75V scale
	* internal impedance is 1kΩ (450mV for the full range)
 * AC and DC currents on 30mA or 75mA scale
	* internal impedance is 1kΩ (450µA for the full range)
 * "M" levels in dB
 * signal levels in dB
	* internal impedance is selectable (75Ω, 150Ω, 600Ω or >10kΩ)
	* -60 to +10dB range (by switching display range -40 to +10dB)

Voltage/current meter is not like an ordinary multimeter.
It only specifies 1kΩ internal resistance and 450mV/450µA for the full range.
In a way mA and V and both 30 and 75 scales are used at the same time.
How to interpret the measurement is dependent on the measurement point.
I.e. if the measurement point says "75V-", then the scale of 75 is used, the unit is V, and the AC/DC knob needs to be manually switched to DC.
And if the measurement point says "30mA~", then the scale of 30 is used, the unit is mA, and the AC/DC knob needs to be manually switched to AC.
Some measurement points can specify larger ranges like "300V~" which means that the scale of 30 is used, but it also needs to be multiplied by 10.

Note that the voltage/current meter can't be used to measure anything
except the intended measurement points!

Current meter is also used for measuring received pilot amplitudes.
Modules specify the pilot frequency and mA.
I.e. "60mA" means that it's a measurement point for measuring a 60kHz pilot.
The measurement is not interpreted neither as current or voltage,
and it depends on the specific module.
Sometimes these measurements can be confusing because of all that.

Signal generator output level is selectable between
-17.4dB, -7dB, -3.5dB, 0dB and +8.7dB.
Its internal resistance is 600Ω.

### Design

![PCB1 Component Side](7108-01-front.lowres.jpg)

