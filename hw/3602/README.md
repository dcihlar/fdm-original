## 3602

Transition filter.

A passive filter with multiple stop-bands.

The stop-bands follow:

| Stop-band | Bandwidth |
|-----------|-----------|
| 58.2kHz   | 1.5kHz    |
| 60.165kHz | 50Hz      |
| 62.0kHz   | 400Hz     |
| 71.6kHz   | 1.6kHz    |
| 91.4kHz   | 2.2kHz    |
| 104.7kHz  | 1.0kHz    |
| 107.8kHz  | notch     |
| 112.0kHz  | 3.6kHz    |

Module can be configured by connecting 212-213 bridge (on PCB3):

| Signal frequency    | Bridge 212-213 |
|---------------------|----------------|
| 108.15kHz (3850Hz)  | closed         |
| 108.175kHz (3825Hz) | opened         |

### Specifications

 * Frequency range: 10kHz - 600kHz

### Connections

```
3602.05 = in_A
3602.07 = in_B
3602.08 = in_shield

3602.09 = +
3602.10 = +
3602.11 = +
3602.12 = +
3602.13 = +
3602.14 = +
3602.15 = +
3602.16 = +
3602.17 = +
3602.18 = +

3602.19 = out_A
3602.21 = out_B
3602.23 = out_shield
```

### Design

![Module Cover](3602-03-cover.lowres.jpg)

![PCB1 Component Side](3602-01-front.lowres.jpg)

![PCB2 Component Side](3602-11-front.lowres.jpg)

![PCB3 Component Side](3602-21-front.lowres.jpg)

![PCB4 Component Side](3602-31-front.lowres.jpg)

![PCB5 Component Side](3602-41-front.lowres.jpg)

![PCB6 Component Side](3602-51-front.lowres.jpg)

![Schematic 1](3602-06-schematic.png)

