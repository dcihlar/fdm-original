## 1003

Main oscillator board.

1024kHz is divided by 2^3^.
Each ring modulator is used as one divide-by-two stage.

This board connects directly to 1122.
For more details see its documentation.


### Connections

```
1003.12 = +
1003.14 = VEE

1003.8  → 1122.10
1003.22 → 1122.2
1003.23 → 1122.3
1003.24 → 1122.4
1003.10 → 1122.8
```

### Design

![Module Cover](1003-03-cover.lowres.jpg)

![PCB1 Component Side](1003-01-front.lowres.jpg)

![PCB1 Traces](1003-05-traces.lowres.jpg)

![Schematic 1](1003-06-schematic.png)

