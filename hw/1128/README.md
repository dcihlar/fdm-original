## 1128

114kHz generator.

The input signal from 1121 is filtered and divided by 2 to get 114kHz.

Its output doesn't have a regulated amplitude so
it needs 12172 for a stable output.

### Specifications

 * Input frequency range: 222 - 232kHz
 * Output frequency range: 111 - 116kHz

### Connections

```
1128.12 = + (option 1)
1128.25 = + (option 2)
1128.14 = VEE

1128.2 = in+ (shield)
1128.4 = in-

1128.15 = out_a
1128.16 = out_b
```


### Design

![Module Cover](1128-03-cover.lowres.jpg)

![PCB1 Component Side](1128-01-front.lowres.jpg)

![PCB1 Traces](1128-05-traces.lowres.jpg)

![Schematic 1](1128-06-schematic.png)

