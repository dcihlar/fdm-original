## 1505

75V AC generator for phone ring signalling.

### Specifications

 * Input:
   * 24V
   * < 100mA (no load)
 * Output:
   * Resistance: 35Ω
   * Frequency: 25 - 30Hz
   * Maximum load: 1kΩ
   * Voltage (no load): 60V RMS (170Vpp)
   * Voltage (1kΩ load): 45V RMS (127Vpp)

### Connections

```
1505.12 = +
1505.14 = VEE

1505.16 = test+
1505.17 = test-

1505.23 = out N
1505.24 = out L
```

### Design

![Module Cover](1505-03-cover.lowres.jpg)

![PCB1 Component Side](1505-01-front.lowres.jpg)

![PCB1 Traces](1505-05-traces.lowres.jpg)

![Schematic 1](1505-06-schematic.png)

