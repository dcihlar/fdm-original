## 2108

84.08kHz oscillator.

It is further buffered by 2412 and then used as a PG pilot frequency.

### Operation

The output frequency is generated from 1345.28kHz crystal by deviding its frequency by 16.

### Connections

```
2108.12 = +
2108.14 = VEE

2108.15 = out_A
2108.17 = out_B
```

