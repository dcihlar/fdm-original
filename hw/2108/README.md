## 2108

84.08kHz oscillator.

It is further buffered by 2412 and then used as a PG pilot frequency.

### Operation

The output frequency is generated from 1345.28kHz crystal by deviding its frequency by 16.

### Connections

```
2108.12 = +
2108.14 = VEE

2108.15 = out_A
2108.17 = out_B
```


### Design

![Module Cover](2108-03-cover.lowres.jpg)

![PCB1 Component Side](2108-01-front.lowres.jpg)

![PCB1 Traces](2108-05-traces.lowres.jpg)

![Schematic 1](2108-06-schematic.png)

