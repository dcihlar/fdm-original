## 86001

Microtelephone mute/unmute switch.

It just connects or disconnects pairs of pins:

 * 2, 5
 * 3, 6
 * 15, 18
 * 16, 19

The pairs are connected in DL position,
and disconnected in KU position.

The module is inserted between 7301 and 8316.


### Connections

```
86001.12 = +

86001.02 = sw1a
86001.05 = sw1b

86001.03 = sw2a
86001.06 = sw2b

86001.15 = sw3a
86001.18 = sw3b

86001.16 = sw4a
86001.19 = sw4b
```
