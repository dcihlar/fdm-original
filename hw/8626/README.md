## 8626

Used for measuring pilots from PGs (84.0xkHz on 4505) and
gains on AGCs.

It's also used for signalling problems on the said measurement points.

This board is very similar to 8628.

### Connections

```
8626.16 = +
8626.18 = VEE

8626.09 = pilsw_out
8626.08 = pilsw1
8626.04 = pilsw2
8626.05 = pilsw3
8626.06 = pilsw4
8626.07 = pilsw5
8626.11 = pilsw6
8626.10 = pilsw7
8626.12 = pilsw8
8626.13 = pilsw9
8626.14 = pilsw10

8626.20 = ampsw0
8626.24 = ampsw1
8626.23 = ampsw2
8626.22 = ampsw3
8626.21 = ampsw4
8626.25 = ampsw5
8626.26 = ampsw6
8626.27 = ampsw7
8626.28 = ampsw8
8626.29 = ampsw9
8626.30 = ampsw10

8626.01 = rly
8626.02 = rlysw_A
8626.03 = rlysw_B
```


### Design

![Module Cover](8626-03-cover.lowres.jpg)

![PCB1 Component Side](8626-01-front.lowres.jpg)

![PCB1 Traces](8626-05-traces.lowres.jpg)

![Schematic 1](8626-06-schematic.png)

