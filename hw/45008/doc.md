## 45008

This module is used for configuring the upstream link.

I.e. PGs can be connected directly to the twinax connectors,
or chained through 23015 or merged into double PGs,
and at the same time input/output gain can be set.

The second schematic represents how the two of these modules can be connected
to allow converting a PG into V12-A or joining two PGs into V24.
Note that it only represents data paths.
It's not an implementation schematic!

Upper switch selects conversion mode:

 * V12-A: PG is mirrored to PG-A,
 * V12-B: PG is simply forwarded as it is already PG-B,
 * V24: two PGs are joined into a 24 channel group.

Lower switch selects attenuation or selects raw PG:

 * 0/0: adding gain to outgoing PG and an attenuator to incoming PG,
 * PGo: forwarding PG as-is directly from 4505 to the twinax connectors,
 * -45/-15: adding attenuators to PGs.

For more details see "RM-4A upstream interface".

The amplifier within this module is the same as in 4508.


### Connections

**Note:** W is a coaxial cable identifier

```
45008P.19 = +
45008N.12 = +
45008N.14 = VEE

45008N.01 = W9+
45008N.02 = W9-

45008N.05 = W2+
45008N.06 = W2-

45008N.09 = W6+
45008N.10 = W6-

45008N.15 = W15+
45008N.16 = W15-

45008N.19 = W13+
45008N.20 = W13-

45008N.23 = W3+
45008N.25 = W3-

45008P.01 = W8+
45008P.02 = W8-

45008P.03 = W7+
45008P.04 = W7-

45008P.05 = W12+
45008P.06 = W12-

45008P.07 = W11+
45008P.08 = W11-

45008P.09 = W17+
45008P.10 = W17-

45008P.11 = W16+
45008P.12 = W16-

45008P.13 = W14+
45008P.14 = W14-

45008P.15 = W23+
45008P.16 = W23-

45008P.17 = W22+
45008P.18 = W22-

45008P.20 = W21+
45008P.21 = W21-

45008P.22 = W20+
45008P.23 = W20-

45008P.24 = W19+
45008P.25 = W19-

45008P.26 = W18+
45008P.27 = W18-

45008P.28 = W26+
45008P.29 = W26-

45008P.30 = W25+
45008P.31 = W25-

45008P.32 = W24+
45008P.33 = W24-

```
