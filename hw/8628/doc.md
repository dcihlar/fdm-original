## 8628

Used for measuring pilots from PGs (84.0xkHz on 4505) and
60/413kHz on dual PGs.

It's also used for signalling problems on the said measurement points.

This board is very similar to 8626.

### Connections

```
8628.16 = +
8628.18 = VEE

8628.09 = pilsw_out
8628.17 = pilsw0
8628.08 = pilsw1
8628.04 = pilsw2
8628.05 = pilsw3
8628.06 = pilsw4
8628.07 = pilsw5
8628.11 = pilsw6
8628.10 = pilsw7
8628.12 = pilsw8
8628.13 = pilsw9
8628.14 = pilsw10
8628.15 = pilswX

8628.31 = pil60sw

8628.01 = rly
8628.02 = rlysw_A
8628.03 = rlysw_B
```
