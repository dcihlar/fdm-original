## 3231 - 3235

These modules are V120/3 PG upconverters.

PG is mixed with a carrier signal and the result is PG shifted into higher frequencies.

Output gain is adjustable.

They also forward the carrier to 3236 - 3240.

### Connections

```
3231.12 = +
3231.13 = VEE

3231.06 = carrier_in_A
3231.07 = carrier_in_B
3231.08 = carrier_out_A
3231.09 = carrier_out_B

3231.17 = PGin_+
3231.15 = PGin_A
3231.16 = PGin_B

3231.01 = out+
3231.02 = out-
```
