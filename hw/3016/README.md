## 3016 - 3018

First level up/down-converters.

They are connected via 4804 (4-wire) or 5601 (2-wire) directly to outside network.

Their outputs are grouped into 3115 - 3118 to perform the next level of conversion.

Modules differ in carrier frequency:

 * 3016 = 12.00kHz
 * 3017 = 16.00kHz
 * 3018 = 20.00kHz

Upstream frequency is shifted up by the carrier frequency.
I.e. 3016 will shift an audio range of 0..4kHz to 12..16kHz.

Jumper 113-114 needs to be connected for channel 6!
It increases the signal carrier gain so it can pass through
the pilot attenuator.

Outgoing and incoming signals have measurement points (M) connected
directly to audio output and input respectively.
Therefore measurement point levels are exactly the same as for the audio,
but impedance depends on the line impedance.
If a channel is connected (A jumper plugged in) then it can be measured
only with ≥10Ω meter impedance,
but if a channel is disconnected,
then the meter must have 600Ω input impedance.
Note that the impedance can be undefined if a telephone is on hook!

Direction notation is in reference to audio (A).
![o->-](sym-output.svg) denotes outgoing audio (towards a telephone), and
![o-<-](sym-input.svg) denotes incoming audio (from a telephone).

### Operation

Audio is shifted by carrier.

Bell has its own carrier within audio channel.
When detected it is signaled by turning a relay on.
It is also sent when bell tx is set high.

### Specifications

 * Audio input: -2Np (150mVpp)
 * Audio output: 1Np (6Vpp)
 * Audio impedance: 600Ω
 * Audio measurement input: -2Np (150mVpp)
 * Audio measurement output: 1Np (6Vpp)
 * Audio measurement impedance (on meter):
   * 600Ω when disconnected from a telephone
   * ≥10kΩ when connected to a telephone
 * Shifted input: -3.9Np (44mVpp)
 * Shifted output: -4Np (40mVpp)
 * Shifted impedance: 600Ω
 * Carrier: 0.25Np (2.8Vpp)
 * Power consumption: 750mW
 * Bell tone source: 3.825kHz, 0.25Np (2.8Vpp), -12V biased
 * Injected bell tone: -0.5Npm0 (-4.3dBm0)
   * On the shifted output it appears as -4 - 0.5Np = -3.5Np (11.7mVrms)
   * On a PG output it appears as -4.2 - 0.5Np = -4.7Np (3.52mVrms)

### Connections

For connections with 4804/5601 see 4804.

```
3016.12 = +
3016.14 = VEE

3016.1  = audio tx A
3016.2  = audio tx B
3016.3  = audio tx middle

3016.6  = shifted input A
3016.7  = shifted input B (shield)

3016.9  = bell rx A
3016.10 = bell rx B

3016.16 = bell carrier phase 1
3016.17 = bell carrier phase 2

3016.18 = bell tx

3016.19 = carrier frequency input A
3016.20 = carrier frequency input B (shield)

3016.21 = shifted output A
3016.22 = shifted output B (shield)

3016.24 = audio rx A
3016.25 = audio rx B
```

### Design

![Module Cover](3016-03-cover.lowres.jpg)

![PCB1 Component Side](3016-01-front.lowres.jpg)

![PCB2 Component Side](3016-11-front.lowres.jpg)

![PCB1 Traces](3016-05-traces.lowres.jpg)

![PCB2 Traces](3016-15-traces.lowres.jpg)

![Schematic 1](3016-06-schematic.png)

