## 1120

1120 is a part of the clock generator.

Distorts input signal (4kHz) so it can be multiplied later.
Output signal appears to be double the input frequency in DC spikes when disconnected from other modules.

Input signal is received from 1122.

It's connected to:

 * 12160
 * 12161
 * 12162


### Connections

Note: connected boards use either `out1` or `out2`.

```
1120.12 = +
1120.14 = VEE

1120.2  = in+
1120.4  = in-

1120.7  = out1- → 1216*.7
1120.8  = out1+ → 1216*.8
1120.9  = out2+ → 1216*.9
1120.10 = out2- → 1216*.10
```

### Design

![Module Cover](1120-03-cover.lowres.jpg)

![PCB1 Component Side](1120-01-front.lowres.jpg)

![PCB1 Traces](1120-05-traces.lowres.jpg)

![Schematic 1](1120-06-schematic.png)

