## 1503

3.825kHz oscillator.

Output level is 3Vpp.

### Connections

```
1503.12 = +
1503.14 = VEE

1503.16 = measure-
1503.18 = measure+

1503.20 = out1-
1503.21 = out2-
1503.22 = out+
1503.23 = out+
1503.24 = out3-
1503.25 = en_out3
```
