## 7301

4 channel service phone line handler.

Four knobs are used to put the four phone lines into the call-waiting state.
When a call signal is received, the knob for that channel will light up.
The call is answered by turning the knob and connecting the phone line to the hybrid.

The bell alarm can be turned on and off with the fifth knob.

7301 is like 83024, but only for one channel.
But unlike 83024, it needs an external 8316 for the actual phone.


### Connections

```
7301.12 = +
7301.14 = VEE
7301.16 = 75VAC_A
7301.17 = 75VAC_B

7301.1  = 4°_A
7301.2  = 4°_B
7301.3  = 3°_A
7301.4  = 3°_B
7301.5  = 2°_A
7301.6  = 2°_B
7301.8  = 1°_A
7301.9  = 1°_B

7301.11 = B_OUT
7301.13 = S_IN

7301.18 = any_bell_out
7301.23 = cond_any_bell_out

7301.19 = ext_tune_A
7301.20 = ext_tune_B

7301.21 = Audio_io_A
7301.22 = Audio_io_B

7301.24 = Audio_oi_A
7301.25 = Audio_oi_B
```

### Design

![Module Cover](7301-03-cover.lowres.jpg)

![PCB1 Component Side](7301-01-front.lowres.jpg)

![PCB2 Component Side](7301-11-front.lowres.jpg)

![PCB1 Traces](7301-05-traces.lowres.jpg)

![PCB2 Traces](7301-15-traces.lowres.jpg)

![Schematic 1](7301-06-schematic.png)

