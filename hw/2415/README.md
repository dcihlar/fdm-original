## 2415

60kHz buffer with constant-amplitude output.

It also has a detector for valid output range.

It is used for detecting and injecting 60kHz pilot signal by 23015.

The input signal source is 1121 (pins 7 and 8).


### Connections

```
2415.12 = +
2415.14 = VEE

2415.5  = 2415.6

2415.2  = in-
2415.3  = in+

2415.16 = measure-
2415.18 = measure+

2415.20 = out1-
2415.21 = out2-
2415.22 = out+
2415.23 = out+
2415.24 = out3-
2415.25 = en_out3
```

### Design

![Module Cover](2415-03-cover.lowres.jpg)

![PCB1 Component Side](2415-01-front.lowres.jpg)

![PCB1 Traces](2415-05-traces.lowres.jpg)

![Schematic 1](2415-06-schematic.png)

