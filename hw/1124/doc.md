## 1124

Generates 108kHz from 12kHz.

It also has an additional output with harmonics for an another clock module.

### Connections

```
1124.12 = +
1124.14 = VEE

1124.3  = in12kHz_A
1124.4  = in12kHz_B

1124.1  = out108kHz-
1124.2  = out108kHz+

1124.20 = out2-
1124.20 = out2+
```
