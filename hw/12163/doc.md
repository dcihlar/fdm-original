## 12163 - 12166

These modules are a part of the clock generator.

They are pretty similar to 12160 - 12162.

Input is taken from 1121.

Output frequencies are:

 * 12163 = 21 × 4000 =  84.00kHz
 * 12164 = 24 × 4000 =  96.00kHz
 * 12165 = 27 × 4000 = 108.00kHz
 * 12166 = 30 × 4000 = 120.00kHz

Base frequency of 4kHz is visible as an envelope
of the output signal.

### Connections

Notes:

 * depending on a board either `in1` is used or `in2`,
 * `direct_drive` is not used.

```
12163.12 = +
12163.14 = VEE

12163.2  = direct_drive+
12163.4  = direct_drive-

12163.5  = 12163.6

12163.7  = in1-
12163.8  = in1+
12163.9  = in2+
12163.10 = in2-

12163.16 = measure-
12163.18 = measure+

12163.20 = out1-
12163.21 = out2-
12163.22 = out+
12163.23 = out+
12163.24 = out3-
12163.25 = en_out3
```
