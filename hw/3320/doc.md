## 3320

An amplifier with discretely adjustable gain.

This module is heavily shielded.

Its purpose is _unknown_.
It's complementary to 3336.


### Specifications

 * Frequency range: 250kHz - 600kHz
 * Gain: 3.6 - 4.5
 * Power consumption: 170mW

### Connections

```
3320.12 = +
3320.14 = VEE

3320.01 = +
3320.02 = +
3320.25 = +

3320.22 = in_shield (+)
3320.23 = in
3320.24 = in_shield (+)

3320.04 = out_shield (isolated)
3320.05 = out
3320.06 = out_shield (isolated)
```
