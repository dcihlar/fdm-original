## PSU

24V 5.5A AC-DC power supply.

It also has a over-voltage protection unit, and it is current limited.

Relay B3 signals presence of the mains supply,
and B2 signals the valid output voltage.

Output voltage can be adjusted with the trimmer on the front panel (R29).
Other adjustments are internal.

R11 adjusts over-voltage protection,
and R26 adjusts current limit.

When an over-voltage condition occurs output will be shut down via B1
and shorted to drain the output capacitors.

### Connections

```
PSU.K6a = GND
PSU.K1a = 220VAC N
PSU.K1b = 220VAC L
PSU.K2a = 220VAC N
PSU.K2b = 220VAC L

PSU.P1a = +B=+24V (GND)
PSU.P1b = -B=-24V
PSU.P2a = +B
PSU.P2b = -B
PSU.P3b = -B1
PSU.P4b = -B1

PSU.P3a = sl
PSU.P4a = k'
PSU.P5a = b'
PSU.P5b = S(GL)O'
PSU.P6a = GL
PSU.P6b = füw
```

### Design

![Module Cover](PSU-03-cover.lowres.jpg)

![PCB1 Component Side](PSU-01-front.lowres.jpg)

![PCB2 Component Side](PSU-11-front.lowres.jpg)

![PCB1 Traces](PSU-05-traces.lowres.jpg)

![PCB2 Traces](PSU-15-traces.lowres.jpg)

![Schematic 1](PSU-06-schematic.png)

