## 12172

114kHz buffer.

This buffer stabilizes amplitude of the 114kHz carrier received from 1128.

Its output is used as a carrier for 3242.

### Specifications

 * Frequency range: 110 - 119kHz
 * Output amplitude: 3Vpp

### Connections

```
12172.25 = +
12172.14 = VEE

12172.5  = in_A
12172.6  = in_B

12172.23 = out_A
12173.22 = out_B

12173.19 = measure-
12173.20 = measure+

12173.16 = rly_en
```
