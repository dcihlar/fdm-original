## 3336

The purpose of this module is _unknown_.

This module is heavily shielded.

It appears to be an amplifier with discretely adjustable gain and filtering for V120/3 complementary to 3320.


### Connections

Note: this module is installed upside down, so the pins can be reversed!

```
3336.12 = +
3336.14 = VEE

3336.07 = ?
3336.11 = ?

3336.04 = in+ (shield)
3336.05 = in-
3336.06 = in+ (shield)

3336.22 = out1+ (shield)
3336.23 = out1- (shield)
3336.24 = out1+ (shield)

3336.20 = out2+
3336.21 = out2-
```

### Design

![Module Cover](3336-03-cover.lowres.jpg)

![PCB1 Component Side](3336-01-front.lowres.jpg)

![PCB1 Traces](3336-05-traces.lowres.jpg)

![Schematic 1](3336-06-schematic.png)

