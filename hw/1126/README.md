## 1126

1126 is a part of clock generation subsystem.

Two input frequencies are mixed together and the result is buffered without any filtering.

The first input (pins 20-21) frequency is 24kHz (defined by the band-pass filter),
and the second one isn't fixed to any specific frequency.

### Connections

```
1126.12 = +
1126.14 = VEE

1126.20 = in1+
1126.21 = in1-

1126.16 = in2_A
1126.18 = in2_B

1126.08 = out-
1126.10 = out+
```

### Design

![Module Cover](1126-03-cover.lowres.jpg)

![PCB1 Component Side](1126-01-front.lowres.jpg)

![PCB1 Traces](1126-05-traces.lowres.jpg)

![Schematic 1](1126-06-schematic.png)

