## 3335

Outgoing SG1 downconverter and amplifier.

This module is heavily shielded, and complementary to 3319.
It is similar to 3336, but with added downconversion.

The amplifier has a discretely adjustable gain.

Output signal is a mirror image of the input signal,
shifted by 612kHz.
So an input frequency of 312kHz is shifted to 300kHz,
and 552kHz is shifted to 60kHz.

The resulting signal is the lower part of SG1+2,
generated from an input SG.

### Specifications

 * Carrier: 612kHz
 * Input: 47 - 302kHz
 * Output: 565 - 310kHz

### Connections

Note: this module is installed upside down, so the pins can be reversed!

```
3335.1  = + (chasis)
3335.2  = +
3335.12 = +
3335.14 = VEE
3335.25 = + (chasis)

3335.4  = in+
3335.5  = in-
3335.6  = in+

3335.7  = ?
3335.10 = ?+
3335.11 = ?

3335.16 = carrier+
3335.17 = carrier-
3335.18 = carrier+

3335.20 = measure+
3335.21 = measure-

3335.22 = out+
3335.23 = out-
3335.24 = out+
```

### Design

![Module Cover](3335-03-cover.lowres.jpg)

![PCB1 Component Side](3335-01-front.lowres.jpg)

![PCB1 Traces](3335-05-traces.lowres.jpg)

![Schematic 1](3335-06-schematic.png)

