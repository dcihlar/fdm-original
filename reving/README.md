This was generated with the help of [RevIng](https://bitbucket.org/dcihlar/reving/).

All rack levels (subracks) were scanned automatically.

Some connectors were scanned semi-automatically.
They were added manually to a json,
and then probed interactively.
RevIng was instructing which pin on a connector to ground
and than it scanned the whole subrack again to find where
that pin of the connector is connected.

Connections between subracks were found mostly manually,
and documented in txt files.
