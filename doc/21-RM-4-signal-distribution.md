### Signal distribution

![Signal distribution graph](RM-4-signal-distribution.svg)

All modules are full duplex, implementing separate circuitry for both incoming and outgoing signals.

To reduce the number of required signals the FDM is implemented in two layers.
The lower layer is made of modules 3016, 3017 and 3018 which are offsetting the frequency by 12, 16 and 20kHz respectively.
The upper layer is made of modules 3115 - 3118 which are further offsetting the frequency by 120 - 84kHz,
but a mirror image is used so that the input frequency is subtracted from the carrier frequency.

The output is connected to the lower layer modules via modules 4804 or 5601.
4804 is just an attenuator for 4-wire audio, while 5601 allows a typical 2-wire telephone link.
5601 can be also rewired to act as 4804.

4505 is a buffer for the FDM links, but adds and monitors a 84.08kHz pilot signal.
If the pilot signal isn't present, than the whole PG is not properly connected.

23015 implements output amplifier for PG, and injects and monitors a 60kHz pilot signal.
23015 is used when two PGs are merged into a double sized 24-channel PG with 3242.

There is also another variant of 4505 that just injects 84.08kHz pilot signal, without monitoring it.
In that case 8704 is used for monitoring the pilot signal.
8704 can also regulate AGC to get PG amplitude stable when AM is used on upstream.

#### RM-4 upstream interface

RM-4 uses 8704 for detecting 84.08kHz pilot signals,
and 4505 modules can only inject 84.08kHz pilot signals.

There are no other options or features.
Although 8704 supports AGC that feature is not used.

#### RM-4A upstream interface

RM-4A has a configurable upstream interface.

The interface is configured by four 45008 modules and two 3242 modules.
Each PG has its dedicated 45008, and two PGs share one 3242.

45008 has switches for rerouting the signals,
and 3242 is used to either convert one PG-B into PG-A or
to join two PGs into a 24 channel group.

The lower switch configures attenuation/gain or
connects PG from 4505 directly to the twinax connectors.

The upper switch selects PG-A, PG-B or 24 channel PG mode.
PG-B will just insert 23015 in the path between 4505 and the twinaxes.

The diagram below represents data path options.

![Upstream interface signal path](RM-4A-PG-management.svg)

Note that selecting V12-A on both PGs or
selecting V24 on only one PG is an invalid configuration!
In that case the odd PG will take precedence.

Those options are only available if the attenuation switch is set to "V12-A/B,V24" mode.
Otherwise 4505 is connected directly to the twinaxes.
The attenuator diagram follows:

![Upstream interface signal path](RM-4A-PG-management-attenuation.svg)

Note that 23015 will be used in all cases except when 4505 is connected directly to the twinaxes.
