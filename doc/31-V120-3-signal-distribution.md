### Signal distribution

V120/3 has multiple completely independent parts,
all described in the chapters below.


#### PGs to SG1+2

![PGs to SG1+2 signal distribution graph](V120-3-signal-distribution.svg)

This is the main part which merges 10 PGs into a large SG1+2,
called SGU/RRU on the front panel.

Modules 3231 - 3235 shift 5 outgoing PGs to their appropriate frequency ranges within a SG.
Shifted PGs are then merged with 4104 into the resulting SG.
This is done for PG1-5 and PG6-10 separately to get SG1 and SG2 respectively.
SG1 is then shifted into the lower range by 3335, and SG2 is just buffered by 3336.
Finally, the output of 3335 and 3336 is joined into SG1+2 by 4931.

4104 and 4202 can also inject and detect a 60kHz pilot signal,
but in V120/3 that feature is not used because the modules aren't directly
connected to the remote link.

Incoming PGs are extracted from SG1+2 in the reverse manner by different modules.
4931 is still directly connected to SGU,
but it only passively forwards the signal to 3319 and 3320
which extract SG1 and SG2 respectively.
Each SG is then buffered by 4202 and split to 3236 - 3240
which then extract PG1-5.

The 60kHz pilot signal is injected by 4931 into the outgoing link,
and extracted from the incoming link.
The extracted pilot signal is then detected by 2303.


#### PG1+2 splitters

To handle double PGs (PG1+2) V120/3 has 5 independent splitters available.
Note that PGs are split in both ways.

After PG1+2 links are split by splitters,
the resulting PGs can then be connected for merging into a SG.

![PG splitters signal distribution graph](V120-3-PG-splitters.svg)


#### Transition filters

Transition filters (3602) are used for PGs that just need to be relayed further
(i.e. a relay station forwarding a complete PG directly from one station to another).
It would be extremely inefficient to demodulate individual audio channels from a PG
just to modulate them again so the PG can be forwarded.

A PG from a V120/3 can't be simply connected to another V120/3 because
that PG still has neighboring PGs within.
Demodulators don't have a sharp filters for PGs.
They just have filters for image frequencies.

A pair of transition filters (3602) must be used to forward (transition) a PG,
one for each direction.
Up to 6 PGs can be forwarded as there are 12 3602 modules within a V120/3 rack.

![Transition filters signal distribution graph](V120-3-transition-filters.svg)
