# Overview

RM-4 is low frequency FDM rack that merges 12 audio channels into a single group named "PG" (a short for "primary group").
It can handle up to 4 PGs.

V120/3 is high frequency FDM rack that further merges 10 PGs into a larger group named "SG1+2".
It does it first by merging 5 PGs into "SG" (a short for "super group") two times,
and then merging the two SGs into SG1+2.
120 channels are handled in total (hence 120 in its name).

Chapters "RM-4" and "V120/3" describe the racks in more detail.
