## Markings legend

The following table explains the markings used on the modules:

| Marking                  | Description                                  |
|--------------------------|----------------------------------------------|
| PG                       | Primary Group - containing 12 audio channels |
| V12-A                    | "A" Primary Group                            |
| V12-B                    | "B" Primary Group                            |
| V25                      | 24 channel PG (PG1+2)                        |
| SG                       | Super Group - containing 60 audio channels   |
| SG1...5                  | Up to 5 combined SGs                         |
| SGU / RRU                | 2 combined SGs (SG1+2)                       |
| M                        | Measure point                                |
| S                        | Call/off-hook signal                         |
| A                        | Audio (voice) signal                         |
| ![o-<-](sym-input.svg)   | Input to device                              |
| ![o->-](sym-output.svg)  | Output from device                           |
| ![o-x-](sym-bidir.svg)   | Full-duplex line                             |
| ![-S-▷](sym-pilot.svg)   | Pilot signal                                 |
| ![-S-▶](sym-carrier.svg) | Carrier signal                               |
| ![alarm](sym-alarm.svg)  | Alarm                                        |
| ![O](sym-on.svg)         | On (enabled)                                 |
| ![O.](sym-off.svg)       | Off (disabled)                               |
| ![▷](sym-agc.svg)        | AGC (automatic gain control)                 |
