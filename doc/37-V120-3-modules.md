#### Modules

These are row-by-row positions of the modules in rack:

 1. (5 blank covers hiding jumpers), 8626, 7301, 86001, 8316, 1505
 2. [1003, 1122, ?], 1124, 1125, 1126, 1267, 1268, 1269, 1270, 1271, 1121, 2415, -, 1415, 8515, 8324
 
 3. 3231, 3236, 3232, 3237, 3233, 3238, 3234, 3239, 3235, 3240, 4104, 4202, 3319, 3335, 4931, 2303, 86036, 7108
 4. 3231, 3236, 3232, 3237, 3233, 3238, 3234, 3239, 3235, 3240, 4104, 4202, 3320, 3336, -
 
 5. 4508, 3242, 4508, 3242, 4508, 3242, 4508, 3242, 4508, 3242, [1121], 1128, 12172
 
 6. 3602, 3602, 3602, 3602
 7. 3602, 3602, 3602, 3602
 8. 3602, 3602, 3602, 3602
 
 9. PSU
 
 10. Twin-ax connectors

Instead of jumpers in the 1st level 5× 8704 can be used to get AGC on the
incoming PGs.

The first three clock modules in the 2nd level are usually not used as the
12kHz clock is generated externally.
But if the external 12kHz is not available than the three modules need to
be installed.

3303 is written between 3319 and 3335.
3304 is written between 3320 and 3336.
Maybe 3303 and 3304 are optional modules that go instead of module pairs 3319/3335 and 3320/3336 respectively.

In the 5th level there is an option to use 1121,
but it is not needed as it is already in the clock level.
