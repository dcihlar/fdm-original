### Rack terminals

#### Audio

The picture below displays the pinout for the audio connectors.
Each PG has three audio connectors, each handling four separate audio channels.

The upper four rows are for output signals of each channel and
the lower four rows are for input signals of each channel.

Connectors 2, 5, 8, 11 have ground (+24V) on pin C5.

![Audio connectors](lf-output-pinout.svg)

#### Monitoring

The picture at the end of this section displays the pinout for the monitoring connector.

It is the same for all racks, including V120/3.
So some of the signals may not apply to some variants.
I.e. only the first service line may be used.

Signalization and alarm connections:

| Signal                | Description                                                |
|-----------------------|------------------------------------------------------------|
| FÜw                   | remote signalization group                                 |
| b                     | power loss alarm                                           |
| üw                    | remote signalization                                       |
| r                     | power loss alarm                                           |
| a                     | A-alarm                                                    |
| S                     | 24V signalization power supply                             |
| tr                    | carrier fault alarm                                        |
| S(GL)P                | power supply (external) for GL alarm lamps                 |
| k                     | external acoustic alarm                                    |
| s                     | burned S1 fuse / S1 or S backup power supply failure alarm |
| -B                    | battery (-24V)                                             |
| +B                    | battery (+24V)                                             |
| B1                    | signalization power supply source                          |
| ks1 - ks4             | service phone lines                                        |
| ![bell](sym-bell.svg) | call on a service line                                     |

![Monitoring connector](monitoring-pinout.svg)

#### Ram E1

This is where all PGs on RM-4A are connected.

Note that in some cases a two PGs can be merged into a single link.
That is done by module 3242, and used only if enabled.

PG-P1 and PG-P3 can be used as external PG1 and PG3, replacing the internal ones.
There are jumpers on the bottom of the first subrack for switching between
the internal and external PGs.
The outputs of 4505 and PG-Px are connected directly to the jumpers.

![Ram E1 connector array](RamE1-pinout.svg)

In some cases there are connector for clocking signals below the PG connectors.

A pair of BNC connectors is used for 12kHz clock.

The connectors are connected in parallel
which enables daisy chaining multiple racks on the same clock.

Depending on the rack configuration the ports are either output or input.
If the rack doesn't have its own clock generation,
then an external 12kHz source needs to be connected to one of the ports.
And if the rack has its own clock generation,
then it can drive other racks which don't have it.

There is also an 4kHz twinax connector that can be used in a similar way.
It can either supply 4kHz to the rack if it doesn't have its own 4kHz generator,
or it can drive some other rack.

![Clocking connectors in the connector array](RamE1-clk-pinout.svg)

#### Power supply

It's just a wire coming out that needs to be connected to 230V AC.
