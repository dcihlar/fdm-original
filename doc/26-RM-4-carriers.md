#### Carriers

Carriers for lower mixers:

| Module | Carrier  |
|--------|----------|
| 3016   | 12.00kHz |
| 3017   | 16.00kHz |
| 3018   | 20.00kHz |


Carriers for upper mixers:

| Module | Carrier   |
|--------|-----------|
| 3115   | 120.00kHz |
| 3116   | 108.00kHz |
| 3117   | 96.00kHz  |
| 3118   | 84.00kHz  |


Channel matrix:

| Upper\\Lower | 3016 | 3017 | 3018 |
|--------------|------|------|------|
| 3115         | CH1  | CH2  | CH3  |
| 3116         | CH4  | CH5  | CH6  |
| 3117         | CH7  | CH8  | CH9  |
| 3118         | CH10 | CH11 | CH12 |


Carriers for specific channels:

| Channel | Carrier   |
|---------|-----------|
| CH1     | 108kHz    |
| CH2     | 104kHz    |
| CH3     | 100kHz    |
| CH4     | 96kHz     |
| CH5     | 92kHz     |
| CH6     | 88kHz     |
| CH7     | 84kHz     |
| CH8     | 80kHz     |
| CH9     | 76kHz     |
| CH10    | 72kHz     |
| CH11    | 68kHz     |
| CH12    | 64kHz     |

Note that the channels are mirror images,
and the carrier is at the lower channel frequency.
