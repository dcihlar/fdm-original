# Introduction

This documentation is result of reverse engineering FDM racks made by Iskra in the late 70's.
The focus is mostly on RM-4 and V120/3.

Contained are schematics and descriptions of modules.

![The racks as they were installed in an underground radio relay station](installed-racks.jpg)
