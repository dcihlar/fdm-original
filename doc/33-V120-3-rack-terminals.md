### Rack terminals

![V120/3 terminals](V120-3-pinout.svg)

#### PGs and SGU

PGs are located on the upper part of the terminals array,
and marked with numbers 1 - 10.
They have a pair of twinax connectors, one for each direction.

SGU/RRU pair of twinaxes is located in the lower right corner.
They carry the signal containing all the PGs.

Note that only odd PGs can have AGC.

#### PG1+2 splitters

Groups M12-1 to M12-5 on the lower part of the terminals array are
extracted PG pairs from M12/24-RRU group located below it.
I.e. M12/24-RRU twinax pair 1 will be extracted to two PGs in the M12-1 group.

Each group is composed of PG1 and PG2
(those are the names of PGs within a group).
PG1 will be forwarded as-is (PG-B),
and PG2 will be mirrored to lower frequencies (PG-A).

#### Transition filters

Each filter has its own pair of twinaxes.

The order of the twinax pair is the same as the order of the modules within rack
counted from left to right, top to bottom.
Filter symbol identifies both terminals of each filter.

#### Clocking

A pair of BNC connectors on the lower right is used for 12kHz clock.

The connectors are connected in parallel
which enables daisy chaining multiple racks on the same clock.

For more details see the same part of the RM-4.

#### Monitoring

The monitoring connector is the same as on RM-4.

#### Power supply

It's just a wire coming out that needs to be connected to 230V AC.
