## Frequency ranges

Standard frequency ranges are defined here for convenience:

| Short name   | Full name       | Range               | Impedance | Note                       |
|--------------|-----------------|---------------------|-----------|----------------------------|
| A            | Audio channel   | 0.3 — 3.4kHz        | 600Ω sym. | with 3.820kHz for "S" tone |
| VG           |                 | 12 — 24kHz          | 150Ω sym. | group of three A channels  |
| PG           | B primary group | 60 — 108kHz (image) | 150Ω sym. | preferred                  |
| PG           | A primary group | 12 — 60kHz          | 150Ω sym. | not used                   |
| PG1+2 (PG24) | 24 channel PG   | 6 — 108kHz          | 150Ω sym. |                            |
| SG           | Super group     | 312 — 552kHz        | 75Ω asym. |                            |
| SG1+2        | 120 channels SG | 60 — 552kHz         | 75Ω asym. | SG1 is mirrored            |


### Audio channel

Each audio channel is 4kHz wide,
but that also includes channel separation zones.

Internally a tone of 3825Hz is used as a ring signal.
Because of that the actual usable range for voice is 3.4kHz.

The ring tone is detected and injected by the lower layer FDM modules 3016 - 3018.
