hwdirs=$(dir $(shell find ../hw -name doc.md | sort))
hwmds=$(addsuffix README.md,$(hwdirs))
docmds=$(sort $(wildcard ../doc/[0-9][0-9]-*.md))
docdep=$(wildcard ../doc/*.png ../doc/*.jpg ../doc/*.svg)
mds=$(docmds) modules.md $(hwmds)
template=template.tex
lowres_fmts=jpg png
img_types_lowres=3 1 5
img_types_fullres=6

#
# helper functions
#
# dirname
#  (1) = path
dirname=$(notdir $(patsubst %/,%,$(1)))
# img_fname
#  (1) = path
#  (2) = group
#  (3) = type
#  (4) = description
#  (5) = extension
img_fname=$(1)$(call dirname,$(1))-$(2)$(3)-$(4).$(5)
# img_glob
#  (1) = path
#  (2) = type
#  (3) = extension
img_glob=$(call img_fname,$(1),[0-9],$(2),*,$(3))
# find_imgs_with_type
#  finds images with specific type for a module
#  (1) = path (ending with /)
#  (2) = image type
find_imgs_with_type=\
	$(filter-out %.lowres.jpg, \
		$(sort $(filter %.jpg %.png, \
			$(wildcard $(call img_glob,$(1),$(2),*)))))
# find_imgs_with_types
#  same as find_imgs_with_type, but for multiple types
#  (1) = path (ending with /)
#  (2) = image types
find_imgs_with_types=\
	$(foreach type,$(2),$(call find_imgs_with_type,$(1),$(type)))
# find_lowres_imgs_with_type
#  finds lowres images with specific type
#  (1) = path (ending with /)
#  (2) = image type
find_lowres_imgs_with_type=\
	$(wildcard $(call img_glob,$(1),$(2),lowres.jpg))
# find_lowres_imgs
#  finds all lowres images
#  (1) = path (ending with /)
find_lowres_imgs=\
	$(foreach type,$(img_types_lowres), \
		$(call find_lowres_imgs_with_type,$(1),$(type)))
# find_conv_lowres_imgs
#  finds images that need to be converted
#  (1) = path (ending with /)
find_conv_lowres_imgs=\
	$(foreach ext,$(lowres_fmts), \
		$(patsubst %.$(ext),%.lowres.jpg, \
			$(filter %.$(ext),$(call find_imgs_with_types,$(1),$(img_types_lowres)))))
# find_nonconv_lowres_imgs
#  finds lowres images without originals
#  (1) = path (ending with /)
find_nonconv_lowres_imgs=\
	$(filter-out $(call find_conv_lowres_imgs,$(1)), \
		$(call find_lowres_imgs,$(1)))
# order_by_types
#  order images by specific types
#  (1) = path (ending with /)
#  (2) = types
#  (3) = images
order_by_types=\
	$(foreach type,$(2),$(foreach num,0 1 2 3 4 5 6 7 8 9, \
		$(filter $(call img_fname,$(1),$(num),$(type),%,lowres.jpg),\
			$(3))))
# find_dep_imgs
#  finds all directly required images for a module
#  note that some images are lowres
#  (1) = path (ending with /)
find_dep_imgs=\
	$(call order_by_types,$(1),$(img_types_lowres), \
		$(call find_nonconv_lowres_imgs,$(1)) \
		$(call find_conv_lowres_imgs,$(1))) \
	$(call find_imgs_with_types,$(1),$(img_types_fullres))


#
# generate rsrc_dir
#
# this uses a hack to join directories with :
noop=
space=$(noop) $(noop)
rsrc_dir=$(subst $(space),:,$(hwdirs) ../doc/)


#
# the main rule
#
book: FDM.pdf


#
# detect and define README.md dependencies
#
define readme_depend
$(1)README.md: $(call find_dep_imgs,$(1))
endef
$(foreach hwd,$(hwdirs),$(eval $(call readme_depend,$(hwd))))


#
# other rules
#
clean-readme:
	@echo Cleaning generated READMEs...
	@rm -f $(hwmds)

clean: clean-readme
	@echo Cleaning output files...
	@rm -f FDM.pdf FDM.tex test.pdf
	@echo Cleaning helper files...
	@rm -f $(foreach hwd,$(hwdirs),$(call find_conv_lowres_imgs,$(hwd)))

define lowres_rule
%.lowres.jpg: %.$(1)
	@echo convert $$@
	@convert $$< -resize 600x600 $$@
endef
$(foreach ext,$(lowres_fmts),$(eval $(call lowres_rule,$(ext))))

%/README.md: %/doc.md adddesign.sh
	@echo Update $@
	@./adddesign.sh $@ $^

readme: $(mds)

# test.pdf contains only a first few modules so it is generated faster
test_mds=$(wordlist 1,$(words $(docmds) readme.md 1 2 3),$(mds))
test.pdf: $(template) $(test_mds) $(docdep)
	@echo pandoc $@
	@pandoc --pdf-engine=xelatex --resource-path=$(rsrc_dir) --template "$(PWD)/$(template)" -o $@ $(test_mds)
test: test.pdf

FDM.pdf: $(template) $(mds) $(docdep)
	@echo pandoc $@
	@pandoc --pdf-engine=xelatex --resource-path=$(rsrc_dir) --template "$(PWD)/$(template)" -o $@ $(mds)

FDM.tex: $(template) $(mds)
	@echo pandoc $@
	@pandoc --template "$(PWD)/$(template)" -o $@ $(mds)

.PHONY: book readme test clean clean-readme
